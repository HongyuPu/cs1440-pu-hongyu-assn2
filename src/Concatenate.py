def cat(args):
    for i in range(0, len(args) - 2):  # prints out stuff
        file = open(args[2 + i])
        print(file.read(), end='')
        file.close()
    """concatenate files and print on the standard output"""
    pass


def tac(args):
    for i in range(0, len(args) - 2):
        file = open(args[2 + i])  # opens file
        text = file.readlines()
        for x in range(len(text) - 1, -1, -1):  # prints out stuff
            if x == len(text) - 1:
                print(text[x])  # corrects spacing
            else:
                print(text[x], end='')
        file.close()
    print()
    """concatenate and print files in reverse"""
    pass
