def wc(files):
    for i in range(len(files) - 2):  # works for multiple files
        file = open(files[2 + i])
        lines = file.readlines()  # lines count
        sumWords = 0  # word count
        sumChar = 0  # char count
        for j in range(len(lines)):
            words = lines[j].split(" ")
            sumWords = sumWords + len(words)  # counts words
            for k in range(len(words)):
                characters = list(words[k])  # counts chars
                sumChar = sumChar + len(characters)
        # prints it out formatted
        print('{0:<2} {1:<5} {2:<9}'.format(len(lines), sumWords, sumChar) + str(files[2 + i]))
        file.close()

    """print newline, word, and byte counts for each file"""
    pass
