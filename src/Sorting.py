def sort(args):
    file = open(args[2])
    text = file.readlines()
    text.sort()  # sorts the text
    for i in range(len(text)):  # prints the text
        print(text[i], end='')
    file.close()
    """sort lines of text files"""
    pass


def uniq(args):
    if args[2] == "-c":  # counts number of similar items
        file = open(args[3])
        text = file.readlines()
        words = [text[0]]
        wordsCount = []
        count = 0
        for i in range(len(text)):
            if text[i] not in words:
                words.append(text[i])

        for i in range(len(words)):
            for j in range(len(text)):
                if words[i] == text[j]:
                    count += 1
            wordsCount.append(count)
            count = 0

        for i in range(len(words)):
            print(wordsCount[i],  words[i], end='')
        file.close()
    elif args[2] == "-D":  # prints duplicates
        file = open(args[3])
        text = file.readlines()
        words = []
        for i in range(len(text)):
            for j in range(len(text)):
                if text[i] == text[j] and i != j:
                    words.append(text[i])
        for i in words:
            print(i, end='')
        file.close()
    elif args[2] == "-u":  # prints unique
        file = open(args[3])
        text = file.readlines()
        count = 0
        for i in range(len(text)):
            for j in range(len(text)):
                if text[i] == text[j]:
                    count += 1
            if count > 1:
                count = 0
            else:
                print(text[i], end='')
        file.close()
    else:  # base case
        file = open(args[2])
        text = file.readlines()
        words = [text[0]]
        for i in range(len(text)):
            if text[i] not in words:
                words.append(text[i])
        for i in words:
            print(i, end='')
        file.close()

    """report or omit repeated lines"""
    pass
