def grep(args):

    if args[2] == "-v":  # case for excluding
        search = str(args[3])
        for i in range(len(args) - 4):
            file = open(args[4 + i])
            text = file.readlines()
            for j in range(len(text)):
                if search not in text[j]:
                    print(text[j], end='')
            file.close()
            print()
    else:
        search = str(args[2])  # case for searching terms
        for i in range(len(args) - 3):
            file = open(args[3 + i])
            text = file.readlines()
            for j in range(len(text)):
                if search in text[j]:
                    print(text[j], end='')
            file.close()

    """print lines of files matching a pattern"""
    pass
