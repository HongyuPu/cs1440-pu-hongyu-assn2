def head(args):

    if args[2] == "-n":  # case with specified number
        file = open(args[4])
        text = file.readlines()  # opens files
        number = int(args[3])
        if len(text) > number:  # prints the text
            for i in range(number):
                print(text[i], end='')
        else:
            for i in range(len(text)):
                print(text[i], end='')
        file.close()
    else:
        file = open(args[2])  # default printing
        text = file.readlines()
        if len(text) > 10:
            for i in range(10):
                print(text[i], end='')
        else:
            for i in range(len(text)):
                print(text[i], end='')
        file.close()
    """output the first part of files"""
    pass


def tail(args):  # prints reverse
    if args[2] == "-n":  # specify the number of items
        file = open(args[4])
        text = file.readlines()
        number = int(args[3])
        if len(text) > number:
            for i in range(number, 0, -1):
                print(text[-i], end='')
        else:
            for i in range(len(text)):
                print(text[-i], end='')
        file.close()
    else:  # base case
        file = open(args[2])
        text = file.readlines()
        if len(text) > 10:
            for i in range(10, 0, -1):
                print(text[-i], end='')
        else:
            for i in range(len(text)):
                print(text[-i], end='')
        file.close()
    """output the last part of files"""
    pass
