def cut(args):
    file = open(args[len(args) - 1])
    text = file.readlines()  # opens the file and reads it
    if len(args) == 3:  # case where no -f arguments are given
        for i in text:
            line = i.split(",")
            print(line[0])
    elif len(args) > 3:  # case where -f arguments are given
        if args[2] == '-f':  # checks -f
            numbers = args[3].split(",")  # gets number of columns to print
            for i in range(len(text)):
                for j in range(len(numbers)):
                    if j == len(numbers) - 1:  # removes commas at the end
                        line = text[i].split(",")
                        print(line[int(numbers[j]) - 1].strip('\n'), end='')
                    else:
                        line = text[i].split(",")
                        print(line[int(numbers[j]) - 1].strip('\n') + ",", end='')
                print()

    file.close()
    """remove sections from each line of files"""
    pass


def paste(args):
    length = []
    word = []  # jagged array at first

    for i in range(len(args) - 2):  # reads and puts file into array
        file = open(args[2 + i])
        text = file.readlines()
        word.append(text)
        length.append(len(text))
        file.close()

    for j in range(len(word)):  # Makes perfect array
        for k in range(max(length) - len(word[j])):
            if len(word[j]) < max(length):
                word[j].append("")

    for m in range(len(word[0])):  # prints the files
        print()
        for n in range(len(word)):
            words = word[n][m].strip('\n')
            if n == len(word) - 1:
                print(words,  end='')
            elif word[n][m] is not '':
                print(words,  end='')
                print(',', end='')

    """merge lines of files"""
    pass
